﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadingScene : MonoBehaviour
{
    [SerializeField] TweenPosition gloss;
    [SerializeField] TweenScale title;
    [SerializeField] Image loadingProgress;
    bool loadFake = true;
    public static LoadingScene Instance;
    void Awake()
    {
        if (LoadingScene.Instance == null)
        {
            LoadingScene.Instance = this;
        }
        else
        {
            if (LoadingScene.Instance != this)
            {
                Destroy(LoadingScene.Instance.gameObject);
                LoadingScene.Instance = this;
            }
        }

        gloss.ResetToBeginning();
        gloss.PlayForward();
        title.ResetToBeginning();
        title.PlayForward();
    }

    void Start()
    {
        StartCoroutine("LoadAsyncOperation");
    }

    void Update()
    {
        if (loadFake)
        {
            loadingProgress.fillAmount += Time.deltaTime / 5;
        }
    }

    IEnumerator LoadAsyncOperation()
    {
        yield return new WaitForSeconds(1.5f);
        loadFake = !loadFake;
        yield return new WaitForSeconds(1f);
        loadFake = !loadFake;
        yield return new WaitForSeconds(2.5f);
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(1);
        while (!asyncLoad.isDone)
        {
            if (asyncLoad.progress >= loadingProgress.fillAmount)
            {
                loadingProgress.fillAmount = asyncLoad.progress;
            }
            yield return new WaitForEndOfFrame();
        }
    }
}
