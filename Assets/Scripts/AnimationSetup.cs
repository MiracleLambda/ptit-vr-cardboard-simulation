﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationSetup : MonoBehaviour
{
    [SerializeField] Animator animator;
    [SerializeField] float velocity;
    private void OnValidate() {
        animator = this.GetComponent<Animator>();
    }
    void Start() {
        animator.SetFloat("Speed_f", velocity);
    }
}
