﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GVRGazer : MonoBehaviour
{
    [SerializeField] Image gazeImg;
    float totalGazeTime = 0.75f;
    bool gvrStatus;
    float gvrTimer;
    int rayDistance = 10;
    RaycastHit hit;
    void Start()
    {
        gazeImg.fillAmount = 0;
    }
    void Update()
    {
        if (gvrStatus)
        {
            gvrTimer += Time.deltaTime;
            gazeImg.fillAmount = gvrTimer / totalGazeTime;
        }

        Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0f));

        if (Physics.Raycast(ray, out hit, rayDistance))
        {
            if (gazeImg.fillAmount == 1)
            {
                if (hit.transform.CompareTag("Arrow"))
                {
                    hit.transform.gameObject.GetComponent<Teleport>().GazerTeleport();
                }
                if (hit.transform.CompareTag("PLAY"))
                {
                    SettingsController.Instance.TurnOffIntro();
                }
                if (hit.transform.CompareTag("QUIT"))
                {
                    SettingsController.Instance.Shutdown();
                }
                if (hit.transform.CompareTag("SETTINGS"))
                {
                    SettingsController.Instance.TurnOnSettings();
                }
                if (hit.transform.CompareTag("MAP"))
                {
                    SettingsController.Instance.TurnOnMap();
                }
                if (hit.transform.CompareTag("CREDITS"))
                {
                    SettingsController.Instance.TurnOnCredits();
                }
                if (hit.transform.CompareTag("BACK"))
                {
                    SettingsController.Instance.Back();
                }
                if (hit.transform.CompareTag("NEXT1"))
                {
                    GalleryController.Instance.Next(1);
                }
                if (hit.transform.CompareTag("NEXT2"))
                {
                    GalleryController.Instance.Next(2);
                }
                if (hit.transform.CompareTag("PREV1"))
                {
                    GalleryController.Instance.Prev(1);
                }
                if (hit.transform.CompareTag("PREV2"))
                {
                    GalleryController.Instance.Prev(2);
                }
                if (hit.transform.CompareTag("ON"))
                {
                    SettingsController.Instance.ToggleSoundOff();
                }
                if (hit.transform.CompareTag("OFF"))
                {
                    SettingsController.Instance.ToggleSoundOn();
                }
                GVROff();
            }
        }
    }

    public void GVROn()
    {
        gvrStatus = true;
    }

    public void GVROff()
    {
        gvrStatus = false;
        gvrTimer = 0;
        gazeImg.fillAmount = 0;
    }
}
