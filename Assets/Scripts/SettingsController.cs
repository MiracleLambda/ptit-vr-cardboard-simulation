﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingsController : MonoBehaviour
{
    [SerializeField] GameObject introUI;
    [SerializeField] GameObject settingsUI;
    [SerializeField] GameObject outtroUI;
    [SerializeField] GameObject mapUI;
    [SerializeField] GameObject creditsUI;
    [SerializeField] GameObject soundOnBtn;
    [SerializeField] GameObject soundOffBtn;
    bool isFirstTeleport;
    public static SettingsController Instance;
    public bool IsFirstTeleport { get => isFirstTeleport; set => isFirstTeleport = value; }

    void Awake()
    {
        if (SettingsController.Instance == null)
        {
            SettingsController.Instance = this;
        }
        else if (SettingsController.Instance != this)
        {
            Destroy(SettingsController.Instance.gameObject);
            SettingsController.Instance = this;
        }

        introUI.SetActive(true);
        settingsUI.SetActive(false);
        outtroUI.SetActive(false);

        int isSoundOn = PlayerPrefs.GetInt("Sound", 1);
        if (isSoundOn == 0)
        {
            ToggleSoundOff();
        }
        else
        {
            ToggleSoundOn();
        }
    }

    void Update()
    {
        if (isFirstTeleport)
        {
            outtroUI.SetActive(true);
        }
    }
    public void TurnOffIntro()
    {
        introUI.SetActive(false);
    }
    public void TurnOnSettings()
    {
        settingsUI.SetActive(true);
        introUI.SetActive(false);
    }
    public void TurnOnMap()
    {
        mapUI.SetActive(true);
        introUI.SetActive(false);
    }
    public void TurnOnCredits()
    {
        creditsUI.SetActive(true);
        introUI.SetActive(false);
    }
    public void Back()
    {
        settingsUI.SetActive(false);
        creditsUI.SetActive(false);
        mapUI.SetActive(false);
        introUI.SetActive(true);
    }
    public void ToggleSoundOn()
    {
        soundOnBtn.SetActive(true);
        soundOffBtn.SetActive(false);
        PlayerPrefs.SetInt("Sound", 1);
        SoundController.Instance.PlayIntroAndBGMusic();
    }
    public void ToggleSoundOff()
    {
        soundOnBtn.SetActive(false);
        soundOffBtn.SetActive(true);
        PlayerPrefs.SetInt("Sound", 0);
        SoundController.Instance.StopIntroAndBGMusic();
    }
    public void Shutdown()
    {
        Application.Quit();
        return;
    }
}
