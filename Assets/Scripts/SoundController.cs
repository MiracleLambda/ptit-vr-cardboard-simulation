﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundController : MonoBehaviour
{
    [SerializeField] AudioObject[] audioObjects;
    [SerializeField] AudioSource BGMusic;
    public static SoundController Instance;
    void Awake()
    {
        if (SoundController.Instance == null)
        {
            SoundController.Instance = this;
        }
        else if (SoundController.Instance != this)
        {
            Destroy(SoundController.Instance.gameObject);
            SoundController.Instance = this;
        }
    }
    void Start()
    {
        int isSoundOn = PlayerPrefs.GetInt("Sound", 1);
        if (isSoundOn == 1)
        {
            BGMusic.mute = false;
        }
        else
        {
            BGMusic.mute = true;
        }
    }
    public void PlayOnID(int id)
    {
        int isSoundOn = PlayerPrefs.GetInt("Sound", 1);
        for (int i = 0; i < audioObjects.Length; i++)
        {
            if (isSoundOn == 0)
            {
                audioObjects[i].AudioSource.mute = true;
            }
            else
            {
                if (audioObjects[i].AudioID == id)
                {
                    audioObjects[i].AudioSource.Play();
                }
            }

        }
    }
    public void StopOnID(int id)
    {
        for (int i = 0; i < audioObjects.Length; i++)
        {
            if (audioObjects[i].AudioID != id)
            {
                audioObjects[i].AudioSource.Stop();
            }
        }
    }
    public void StopIntroAndBGMusic()
    {
        audioObjects[10].AudioSource.mute = true;
        BGMusic.mute = true;
    }
    public void PlayIntroAndBGMusic()
    {
        audioObjects[10].AudioSource.mute = false;
        BGMusic.mute = false;
    }
}
