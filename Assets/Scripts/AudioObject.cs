﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioObject : MonoBehaviour
{
    [SerializeField] AudioSource audioSource;
    [SerializeField] int audioID;
    public int AudioID { get => audioID; set => audioID = value; }
    public AudioSource AudioSource { get => audioSource; set => audioSource = value; }
    void OnValidate()
    {
        audioSource = this.GetComponent<AudioSource>();
    }
}
