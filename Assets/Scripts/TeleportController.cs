﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportController : MonoBehaviour
{
    [SerializeField] Teleport[] arrows;
    public static TeleportController Instance;
    void Awake()
    {
        if (TeleportController.Instance == null)
        {
            TeleportController.Instance = this;
        }
        else if (TeleportController.Instance != this)
        {
            Destroy(TeleportController.Instance.gameObject);
            TeleportController.Instance = this;
        }
        Application.targetFrameRate = 60;
    }

    public void ResetAllArrows(int aid)
    {
        for (int i = 0; i < arrows.Length; i++)
        {
            if (arrows[i].ArrowID == aid)
            {
                arrows[i].Mesh.enabled = false;
            }
            else
            {
                arrows[i].Mesh.enabled = true;
            }
        }
    }
}
