﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HumanMovement : MonoBehaviour
{
    [SerializeField] TweenPosition studentTweenPos;
    [SerializeField] int duration;
    float counter;
    [SerializeField] float y;
    [SerializeField] float sum;
    void OnValidate()
    {
        studentTweenPos = GetComponent<TweenPosition>();
    }
    void Start()
    {
        studentTweenPos.ResetToBeginning();
        studentTweenPos.PlayForward();
        counter = 0;
    }

    void Update()
    {
        counter += Time.deltaTime;
        if (counter >= duration)
        {
            counter = 0;
            y = sum - y;
            this.transform.localRotation = Quaternion.Euler(0, y, 0);
        }
    }
}
