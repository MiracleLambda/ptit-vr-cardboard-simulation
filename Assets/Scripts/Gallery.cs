﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gallery : MonoBehaviour
{
    [SerializeField] GameObject[] pictures;
    int counter;
    void Start()
    {
        counter = 15000;
        pictures[0].SetActive(true);
        for (int i = 1; i < pictures.Length; i++)
        {
            pictures[i].SetActive(false);
        }    
    }
    public void ViewNext()
    {
        counter++;
        int tmp = counter % pictures.Length;
        for (int i = 0; i < pictures.Length; i++)
        {
            if (i == tmp)
            {
                pictures[i].SetActive(true);
            }
            else
            {
                pictures[i].SetActive(false);
            }
        } 
    }
    public void ViewPrev()
    {
        counter--;
        int tmp = counter % pictures.Length;
        for (int i = 0; i < pictures.Length; i++)
        {
            if (i == tmp)
            {
                pictures[i].SetActive(true);
            }
            else
            {
                pictures[i].SetActive(false);
            }
        } 
    }
}
