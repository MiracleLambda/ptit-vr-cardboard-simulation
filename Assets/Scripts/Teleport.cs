﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleport : MonoBehaviour
{
    [SerializeField] int arrowID;
    [SerializeField] AudioObject audioObject;
    [SerializeField] MeshRenderer mesh;
    [SerializeField] GameObject gazer;
    public int ArrowID { get => arrowID; set => arrowID = value; }
    public MeshRenderer Mesh { get => mesh; set => mesh = value; }

    void OnValidate()
    {
        if (arrowID != 15)
        {
            audioObject = this.GetComponent<AudioObject>();
            mesh = this.GetComponentInChildren<MeshRenderer>();
        }
    }
    public void GazerTeleport()
    {
        gazer.transform.position = new Vector3(transform.localPosition.x, 0.15f, transform.localPosition.z);
        SettingsController.Instance.IsFirstTeleport = true;
        gazer.GetComponent<GVRGazer>().GVROff();
        int isSoundOn = PlayerPrefs.GetInt("Sound", 1);
        if (isSoundOn == 1)
        {
            gazer.GetComponentInChildren<AudioSource>().mute = false;
        }
        else
        {
            gazer.GetComponentInChildren<AudioSource>().mute = true;
        }
        gazer.GetComponentInChildren<AudioSource>().Play();
        TeleportController.Instance.ResetAllArrows(arrowID);
        if (audioObject != null)
        {
            SoundController.Instance.StopOnID(audioObject.AudioID);
            SoundController.Instance.PlayOnID(audioObject.AudioID);
        }
    }
}
