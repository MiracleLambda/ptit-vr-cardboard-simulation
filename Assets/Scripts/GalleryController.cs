﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GalleryController : MonoBehaviour
{
    [SerializeField] Gallery[] galleries;
    public static GalleryController Instance;
    void Awake()
    {
        if (GalleryController.Instance == null)
        {
            GalleryController.Instance = this;
        }
        else if (GalleryController.Instance != this)
        {
            Destroy(GalleryController.Instance.gameObject);
            GalleryController.Instance = this;
        }
    }
    public void Next(int i)
    {
        galleries[i - 1].ViewNext();
    }
    public void Prev(int i)
    {
        galleries[i - 1].ViewPrev();
    }
}
